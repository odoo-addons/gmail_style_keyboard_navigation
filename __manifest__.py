{
    'name': 'Keyboard navigation',
    'version': '9.0',
    'category': 'Tools',
    'description': """
    This module add some gmail-style keyboard shortcuts, stole from be-cloud.be (Jerome Sonnet)

    On a form, edit mode:  

    + Ctrl + S           :  Save the current object
    + Ctrl + Z           :  Cancel the modification of current object

    On a form, mode view: 

    + Ctrl + Delete      :  Delete the current object
    + Ctrl + N           :  New object
    + Ctrl + D           :  Duplicate the current object
    + Ctrl + E           :  Edit the current object


    On Mac, you can use Command key instead of ctrl key except Ctrl+N one

    todos:

    + / to search
    + Esc to unfocus
    + , to toggle sidemenu
    """,
    "author": "",
    "website": "",
    'depends': ['web'],
    'init_xml': [],
    'data': ['gmail_style_keyboard_navigation_view.xml'],
    'installable': True,
    'active': True,
}
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
